//WAP to find the distance between two points using structures and 4 functions.

#include <stdio.h>
#include <math.h>

struct coordinate
{
    float x,y;
};

typedef struct coordinate Coordinate;

float input();
float process(Coordinate a,Coordinate b);
void output(float distance);

int main()
{
    Coordinate a,b;  //a and b are the co ordinates of 2 points
    a.x=input();
    a.y=input();
    b.x=input();
    b.y=input();
    output(process(a,b));
}
float input()
{
    float f;
    printf("enter your co ordinates of the point in the order of x and then y of the same co ordinate=");
    scanf("%f",&f);
    return f;
}
float process(Coordinate a,Coordinate b)
{
    float distance;
    distance=sqrt(pow((a.x-b.x),2)+pow((a.y-b.y),2));
    return distance;
}
void output(float distance)
{
    printf("the distance between the points a(x,y) and B(x,y) is=%f",distance);
}