//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include <math.h>

float input()
{
    float a;
    scanf("%f",&a);
    return a;
}
float dist(float x1,float x2,float y1,float y2)
{
    float d;
    d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return d;
}
float display(float a)
{
    printf("distance between the two points is:%f",a);
    return a;
}
int main()
{
    float x1,x2,y1,y2,d;
    printf("enter the abscissa of the first point");
    x1=input();
    printf("enter the ordinate of the first point");
    y1=input();
    printf("enter the abscissa of the second point");
    x2=input();
    printf("enter the ordinate of the second point");
    y2=input();
    d=dist(x1,x2,y1,y2);
    display(d);
}