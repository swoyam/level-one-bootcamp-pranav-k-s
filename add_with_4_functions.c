//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

#include <stdio.h>
int input();
int add(float a,float b);
void output(float sum);

int main()
{
    float a,b,sum;
	a=input();
	b=input();
	sum=add(a,b);
	output(sum);
	return 0;
}
	
int input()
{
    float x;
	printf("enter a number");
	scanf("%f",&x);
	return x;
}
int add(float a,float b)
{
    float sum;
    sum=a+b;
	return sum;
}
void output(float sum)
{
     printf("%f",sum);
}